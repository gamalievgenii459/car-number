import telebot
from telebot import types
import glob, os, shutil
import os
from string  import Template

bot = telebot.TeleBot(token)

path = " "
binary_img = b''


#start
@bot.message_handler(commands=['start'])
def send_welcome(message):
    msg = bot.send_message(message.chat.id, 'Здраствуйте ' + message.from_user.first_name+", загрузите картинку")
    bot.register_next_step_handler(msg, process_photo_step)

def info_img(m):
    file_info = bot.get_file(m.photo[len(m.photo) - 1].file_id)
    downloaded_file = bot.download_file(file_info.file_path)

    global path
    global binary_img

    binary_img = downloaded_file
    path = file_info.file_path

@bot.message_handler(content_types=['photo'])
def process_photo_step(m):

    '''Добавлеие кнопок'''
    cid = m.chat.id
    keyboard = types.InlineKeyboardMarkup()
    kb1 = types.InlineKeyboardButton(text="True", callback_data="True")
    kb2 = types.InlineKeyboardButton(text="False", callback_data="False")
    keyboard.add(kb1, kb2)

    '''Ответ бота'''
    answer = "4342432"

    msg = bot.send_message(cid,"Car Number " + answer, reply_markup=keyboard)

    '''Информация для картинки'''
    info_img(m)

@bot.callback_query_handler(func=lambda c: True)
def ans(c):
    cid = c.message.chat.id
    keyboard = types.InlineKeyboardMarkup()
    if c.data == "True":
        bot.edit_message_text(chat_id=c.message.chat.id, message_id=c.message.message_id, text="Image in the True folder")
        bot.answer_callback_query(callback_query_id=c.id, show_alert=False, text="Image in the True folder!")

        src = os.getcwd()+r'/True/' + path
        with open(src, 'wb') as new_file:
            new_file.write(binary_img)

    elif c.data == "False":
        bot.edit_message_text(chat_id=c.message.chat.id, message_id=c.message.message_id, text="Image in the False folder")
        bot.answer_callback_query(callback_query_id=c.id, show_alert=False, text="Image in the False folder!")

        src = os.getcwd() + r'/False/' + path
        with open(src, 'wb') as new_file:
            new_file.write(binary_img)
        print (binary_img)

        removed = str(path).replace('photos/', '').replace('.jpg', '.txt')

        file = open('file.txt', 'a')

        file.writelines(removed)

        file.close()

        sent = bot.send_message(c.message.chat.id, 'Enter car number')
        bot.register_next_step_handler(sent, number_car)

def number_car(message):
    file = open("file.txt", 'r+')

    new_file = "Numbers/"+file.read()

    # Чистка file.txt
    file1 = open("file.txt", 'w')

    car_numbers = open(new_file, 'a')

    car_numbers.write(message.text)

if __name__ == '__main__':
     bot.infinity_polling()
